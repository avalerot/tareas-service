package com.example.tareas.app;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.example.tareas.app.controller.TareasController;
import com.example.tareas.app.repository.Tarea;
import com.example.tareas.app.repository.TareasDAO;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.validation.ConstraintViolationException;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.server.ResponseStatusException;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class TareasAppApplicationTests {

  @MockBean
  private TareasDAO tareasDAO;

  @Autowired
  TareasController tareasController;

  @Autowired
  private MockMvc mockMvc;

  @Rule
  public ExpectedException exceptionRule = ExpectedException.none();

	@Test
	void listar() throws Exception {
    Mockito.when(this.tareasDAO.listar())
            .thenReturn(obtenerMaquetaLista());
    mockMvc.perform(MockMvcRequestBuilders.get("/tareas"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content()
            .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andDo(print())
        .andExpect(jsonPath("$").isNotEmpty())
        .andExpect(jsonPath("$").isArray());
	}

  @Test
  void buscarPorIdOk() throws Exception {
    Mockito.when(this.tareasDAO.buscarPorId(1L))
            .thenReturn(Optional.of(obtenerMaqueta1()));
    mockMvc.perform(MockMvcRequestBuilders.get("/tareas/1"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content()
            .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andDo(print())
        .andExpect(jsonPath("$").isNotEmpty())
        .andExpect(jsonPath("$.id").value(1L))
        .andExpect(jsonPath("$.vigente").value(true))
        .andExpect(jsonPath("$.descripcion").value("Descripcion 1"));

  }

  @Test
  void buscarPorIdFueraDeRango() throws Exception {
    exceptionRule.expect(ConstraintViolationException.class);
    mockMvc.perform(MockMvcRequestBuilders.get("/tareas/-1"))
        .andExpect(MockMvcResultMatchers.status().isBadRequest())
        .andDo(print());
  }

  @Test
  void buscarPorIdNotFound() throws Exception {
    exceptionRule.expect(ResponseStatusException.class);
    mockMvc.perform(MockMvcRequestBuilders.get("/tareas/3"))
        .andExpect(MockMvcResultMatchers.status().isNotFound())
        .andDo(print());
  }

  @Test
  void guardarOk() throws Exception {
    Mockito.when(this.tareasDAO.guardar(any(Tarea.class)))
        .thenReturn(obtenerMaqueta1());
    String tarea = "{\"descripcion\": \"Descripcion 1\", \"vigente\" : true}";
    mockMvc.perform(MockMvcRequestBuilders.post("/tareas/guardar")
        .content(tarea)
        .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content()
            .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andDo(print())
        .andExpect(jsonPath("$").isNotEmpty())
        .andExpect(jsonPath("$.id").value(1L))
        .andExpect(jsonPath("$.vigente").value(true))
        .andExpect(jsonPath("$.descripcion").value("Descripcion 1"));
  }

  @Test
  void guardarNoValido() throws Exception {
    exceptionRule.expect(MethodArgumentNotValidException.class);
    String tarea = "{\"vigente\" : true}";
    mockMvc.perform(MockMvcRequestBuilders.post("/tareas/guardar")
            .content(tarea)
            .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(MockMvcResultMatchers.status().isBadRequest())
        .andExpect(MockMvcResultMatchers.content()
            .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andDo(print());
  }

  private List<Tarea> obtenerMaquetaLista() {
    return Arrays.asList(obtenerMaqueta1(), obtenerMaqueta2());
  }

  private Tarea obtenerMaqueta1() {
    Tarea tarea = new Tarea();
    tarea.setId(1L);
    tarea.setDescripcion("Descripcion 1");
    tarea.setFechaCreacion(LocalDateTime.now());
    tarea.setVigente(true);
    return tarea;
  }

  private Tarea obtenerMaqueta2() {
    Tarea tarea = new Tarea();
    tarea.setId(2L);
    tarea.setDescripcion("Descripcion 2");
    tarea.setFechaCreacion(LocalDateTime.now());
    tarea.setVigente(false);
    return tarea;
  }

}
