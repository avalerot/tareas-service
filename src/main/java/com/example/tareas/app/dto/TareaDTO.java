package com.example.tareas.app.dto;

import java.time.LocalDateTime;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class TareaDTO {

  private Long id;
  @NotNull(message = "parametro requerido")
  @NotBlank(message = "descripcion no puede ser vacio")
  private String descripcion;
  private LocalDateTime fechaCreacion;
  @NotNull(message = "parametro requerido")
  private boolean vigente;
}
