package com.example.tareas.app.service;

import com.example.tareas.app.dto.TareaDTO;
import com.example.tareas.app.repository.Tarea;
import com.example.tareas.app.repository.TareasDAO;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
public class TareasService {

  private static final Log LOGGER = LogFactory.getLog(TareasService.class);
  private final TareasDAO tareasDAO;

  @Autowired
  public TareasService(TareasDAO tareasDAO) {
    this.tareasDAO = tareasDAO;
  }

  public List<TareaDTO> listar() {
    LOGGER.info("Ejecuta servicio listar");
    return this.tareasDAO.listar()
        .stream().map(this::constriurTareasDTO)
        .collect(Collectors.toList());
  }

  @Transactional
  public TareaDTO buscarPorId(Long id) {
    LOGGER.info("Ejecuta servicio buscarPorId");
    return this.tareasDAO.buscarPorId(id)
        .map(this::constriurTareasDTO)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "El registro no existe"));
  }

  @Transactional
  public TareaDTO guardar(TareaDTO tareaDTO) {
    LOGGER.info("Ejecuta servicio guardar");
    Tarea tarea = this.construirTarea(tareaDTO);
    tarea = this.tareasDAO.guardar(tarea);
    return this.constriurTareasDTO(tarea);
  }

  @Transactional
  public TareaDTO actualizar(TareaDTO tareaDTO) {
    LOGGER.info("Ejecuta servicio actualizar");
    Tarea tarea = this.tareasDAO.buscarPorId(tareaDTO.getId())
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
            "El registro que intenta actualizar no existe"));
    tarea.setVigente(tareaDTO.isVigente());
    tarea.setDescripcion(tareaDTO.getDescripcion());
    tarea = this.tareasDAO.actualizar(tarea);
    return this.constriurTareasDTO(tarea);
  }

  @Transactional
  public void eliminar(Long id) {
    Tarea tarea = this.tareasDAO.buscarPorId(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "El registro que esta intentando eliminar no existe"));
    this.tareasDAO.eliminar(tarea);
  }

  private TareaDTO constriurTareasDTO(Tarea tarea) {
    if (tarea == null) {
      return null;
    }
    TareaDTO tareaDTO = new TareaDTO();
    tareaDTO.setId(tarea.getId());
    tareaDTO.setDescripcion(tarea.getDescripcion());
    tareaDTO.setFechaCreacion(tarea.getFechaCreacion());
    tareaDTO.setVigente(tarea.isVigente());
    return tareaDTO;
  }

  private Tarea construirTarea(TareaDTO tareaDTO) {
    Tarea tarea = new Tarea();
    tarea.setId(tareaDTO.getId());
    tarea.setDescripcion(tareaDTO.getDescripcion());
    tarea.setFechaCreacion(tareaDTO.getFechaCreacion());
    tarea.setVigente(tareaDTO.isVigente());
    return tarea;
  }
}
