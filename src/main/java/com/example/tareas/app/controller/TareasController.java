package com.example.tareas.app.controller;

import com.example.tareas.app.dto.TareaDTO;
import com.example.tareas.app.service.TareasService;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
@RequestMapping("tareas")
public class TareasController {

  private static final Log LOGGER = LogFactory.getLog(TareasController.class);
  private final TareasService tareasService;

  @Autowired
  public TareasController(TareasService tareasService) {
    this.tareasService = tareasService;
  }

  @GetMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
  public List<TareaDTO> listar() {
    LOGGER.info("Nueva peticion listar");
    return this.tareasService.listar();
  }

  @GetMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public TareaDTO buscarPorId(
      @Valid @Min(value = 1, message = "El valor mínimo de id es 1")
      @PathVariable("id") Long id) {
    LOGGER.info("Nueva peticion buscar por id");
    return this.tareasService.buscarPorId(id);
  }

  @PostMapping(path = "guardar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<TareaDTO> guardar(@Valid @RequestBody TareaDTO tareaDTO) {
    LOGGER.info("Nueva peticion guardar");
    return ResponseEntity.ok(this.tareasService.guardar(tareaDTO));
  }

  @PutMapping(path = "actualizar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public TareaDTO actualizar(@Valid @RequestBody TareaDTO tareaDTO) {
    LOGGER.info("Nueva peticion actualizar");
    return this.tareasService.actualizar(tareaDTO);
  }

  @DeleteMapping(path = "{id}/eliminar", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> eliminar(
      @Valid @Min(value = 1, message = "El valor mínimo de id es 1")
      @PathVariable("id") Long id) {
    LOGGER.info("Nueva peticion eliminar");
    this.tareasService.eliminar(id);
    return ResponseEntity.ok("Registro eliminado correctamente");
  }

}
