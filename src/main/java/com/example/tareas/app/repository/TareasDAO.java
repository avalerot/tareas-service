package com.example.tareas.app.repository;

import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

@Repository
@Validated
public class TareasDAO {

  private static final Log LOGGER = LogFactory.getLog(TareasDAO.class);
  private final EntityManager em;

  @Autowired
  public TareasDAO(EntityManager em) {
    this.em = em;
  }

  public List<Tarea> listar() {
    LOGGER.info("Buscando lista de tareas en bd");
    return this.em.createQuery("select t from Tarea t", Tarea.class).getResultList();
  }

  public Optional<Tarea> buscarPorId(
      @Valid
      @NotNull(message = "el id es requerido")
      @Min(value = 1) Long id) {
    LOGGER.info("Buscando tarea con id " + id);
    return Optional.ofNullable(this.em.find(Tarea.class, id));
  }

  public Tarea guardar(Tarea tarea) {
    LOGGER.info("Persistiendo tarea");
    this.em.persist(tarea);
    return tarea;
  }

  public Tarea actualizar(Tarea tarea) {
    LOGGER.info("Actualizando tarea");
    this.em.merge(tarea);
    return tarea;
  }

  public void eliminar(Tarea tarea) {
    LOGGER.info("Eliminando tarea");
    this.em.remove(tarea);
  }
}
