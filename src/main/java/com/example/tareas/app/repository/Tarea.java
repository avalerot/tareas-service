package com.example.tareas.app.repository;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "tareas")
@Getter @Setter @NoArgsConstructor
public class Tarea {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;

  @Column(name = "descripcion")
  private String descripcion;

  @CreationTimestamp
  @Column(name = "fecha_creacion")
  private LocalDateTime fechaCreacion;

  @Column(name = "vigente")
  private boolean vigente;
}
